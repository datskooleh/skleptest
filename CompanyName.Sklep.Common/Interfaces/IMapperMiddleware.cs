﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompanyName.Sklep.Common.Interfaces
{
    public interface IMapperMiddleware
    {
        TDestination Map<TSource, TDestination>(TSource source);

        void Map<T1, T2>(T1 source, T2 destination);
    }
}
