﻿using CompanyName.Sklep.DataAccess.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompanyName.Sklep.Common.Interfaces
{
    public interface IDataAccessContext
    {
        IEnumerable<T2> GetAll<T1, T2>()
            where T1 : class, IEntity
            where T2 : class, IDTO;

        IEnumerable<T2> GetAll<T1, T2>(Func<T2, Boolean> func)
            where T1 : class, IEntity
            where T2 : class, IDTO;

        T2 Find<T1, T2>(Int32 id)
            where T1 : class, IEntity
            where T2 : class, IDTO;

        Boolean Update<T1, T2>(T2 model)
            where T1 : class, IEntity
            where T2 : class, IDTO;

        Boolean Save<T1, T2>(T2 model)
            where T1 : class, IEntity
            where T2 : class, IDTO;

        Boolean Delete<T1, T2>(T2 model)
            where T1 : class, IEntity
            where T2 : class, IDTO;

        Boolean Delete<T>(Int32 id)
            where T : class, IEntity;
    }
}
