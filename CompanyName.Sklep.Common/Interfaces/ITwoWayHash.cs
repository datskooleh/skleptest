﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompanyName.Sklep.Common.Interfaces
{
    public interface ITwoWayHash : IHash
    {
        String Decode(String str);
    }
}
