﻿using CompanyName.Sklep.Common.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace CompanyName.Sklep.Common.Classes
{
    public class PasswordHash : IHash
    {
        public string Encode(String str)
        {
            String hash = null;

            using (var algorithm = new SHA256CryptoServiceProvider())
            {
                var hashBytes = algorithm.ComputeHash(Encoding.UTF8.GetBytes(str));
                hash = BitConverter.ToString(hashBytes);
            }

            return hash;
        }
    }
}
