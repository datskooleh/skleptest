﻿using CompanyName.Sklep.Common.Interfaces;
using System;
using System.Text;

namespace CompanyName.Sklep.Common.Classes
{
    /// <summary>
    /// All can be cracked/deciphered but you can make it a bit harder to do
    /// </summary>
    public class CookieHash : ITwoWayHash
    {
        public String Encode(String str)
        {
            if (String.IsNullOrWhiteSpace(str))
                throw new ArgumentNullException("Value must be set");

            return Convert.ToBase64String(Encoding.UTF8.GetBytes(str));

        }

        public String Decode(String str)
        {
            if (String.IsNullOrWhiteSpace(str))
                throw new ArgumentNullException("Value must be set");

            return Encoding.UTF8.GetString(Convert.FromBase64String(str));
        }
    }
}
