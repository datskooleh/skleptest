using CompanyName.Sklep.Common.Classes;
using CompanyName.Sklep.Common.Interfaces;
using CompanyName.Sklep.Services;
using CompanyName.Sklep.Services.Interfaces;
using CompanyName.Sklep.Services.Services;
using CompanyName.Sklep.Services.Validators;
using CompanyName.Sklep.Web.Validators;
using FluentValidation;
using Microsoft.Practices.Unity;
using System;

namespace CompanyName.Sklep.Web.App_Start
{
    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    public class UnityConfig
    {
        #region Unity Container
        private static Lazy<IUnityContainer> container = new Lazy<IUnityContainer>(() =>
        {
            var container = new UnityContainer();
            RegisterTypes(container);
            return container;
        });

        /// <summary>
        /// Gets the configured Unity container.
        /// </summary>
        public static IUnityContainer GetConfiguredContainer()
        {
            return container.Value;
        }
        #endregion

        /// <summary>Registers the type mappings with the Unity container.</summary>
        /// <param name="container">The unity container to configure.</param>
        /// <remarks>There is no need to register concrete types such as controllers or API controllers (unless you want to 
        /// change the defaults), as Unity allows resolving a concrete type even if it was not previously registered.</remarks>
        public static void RegisterTypes(IUnityContainer container)
        {
            // NOTE: To load from web.config uncomment the line below. Make sure to add a Microsoft.Practices.Unity.Configuration to the using statements.
            // container.LoadConfiguration();
            container.RegisterType<IHash, PasswordHash>();
            container.RegisterType<ITwoWayHash, CookieHash>();
            container.RegisterType<IMapperMiddleware, MapperMiddleware>();

            container.RegisterType<IAccountService, AccountService>();
            container.RegisterType<IArticleService, ArticleService>();
            container.RegisterType<IUserService, UserService>();
            container.RegisterType<IRoleService, RoleService>();

            //register validators
            var validators = AssemblyScanner.FindValidatorsInAssemblyContaining<CreateArticleValidator>();
            validators.ForEach(validator => container.RegisterType(validator.InterfaceType, validator.ValidatorType));

            container.RegisterType<IValidatorFactory, ValidatorFactory>(new ContainerControlledLifetimeManager());

            //this is used by EntityFramework
            container.RegisterType<IDataAccessContext, CompanyName.Sklep.DataAccess.EntityFramework.AppDbContext>();
            //this is used by ADO.NET (currently not supported)
            //container.RegisterType<IDataAccessContext, CompanyName.Sklep.DataAccess.AdoNet.AppDbContext>();
        }
    }
}
