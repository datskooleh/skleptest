﻿using System;

namespace CompanyName.Sklep.Web.Interfaces
{
    public class Token : ICookieObjectIsValid
    {
        /// <summary>
        /// Auth token that generates automatically to contain "junk"
        /// </summary>
        public String At { get; set; }

        /// <summary>
        /// User login
        /// </summary>
        public String Ul { get; set; }

        /// <summary>
        /// User role
        /// </summary>
        public String Ur { get; set; }

        /// <summary>
        /// Date expires
        /// </summary>
        public DateTime CtE { get; set; }

        public Boolean IsValid()
        {
            return !String.IsNullOrWhiteSpace(At) && At.Length == 32
                && !String.IsNullOrWhiteSpace(Ul)
                && !String.IsNullOrWhiteSpace(Ur)
                && DateTime.Now.Ticks < CtE.ToLocalTime().Ticks;
        }
    }
}