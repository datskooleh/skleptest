﻿using CompanyName.Sklep.Web.App_Start;
using CompanyName.Sklep.Web.Filters;
using FluentValidation;
using FluentValidation.Mvc;
using Microsoft.Practices.Unity;
using System;
using System.Web.Mvc;
using System.Web.Routing;

namespace CompanyName.Sklep.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalFilters.Filters.Add(new AuthenticationAttribute("admin"));

            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            ConfigValidation();

            CompanyName.Sklep.Services.Bootstrapper.Configure();
        }

        private void ConfigValidation()
        {
            var container = UnityConfig.GetConfiguredContainer();
            var factory = (IValidatorFactory)container.Resolve(typeof(IValidatorFactory));

            var provider = new FluentValidationModelValidatorProvider(factory);
            provider.AddImplicitRequiredValidator = false;
            ModelValidatorProviders.Providers.Add(provider);
            DataAnnotationsModelValidatorProvider.AddImplicitRequiredAttributeForValueTypes = false;
        }
    }
}
