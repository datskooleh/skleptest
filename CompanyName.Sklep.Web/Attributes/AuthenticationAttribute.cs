﻿using CompanyName.Sklep.Web.Helpers;
using CompanyName.Sklep.Web.Interfaces;
using System;
using System.Net;
using System.Web.Mvc;
using System.Web.Mvc.Filters;

namespace CompanyName.Sklep.Web.Filters
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = false)]
    public class AuthenticationAttribute : FilterAttribute, IAuthenticationFilter
    {
        public String Role { get; set; }
        public AuthenticationAttribute(String role)
        {
            Role = String.Copy(role);
        }

        public void OnAuthentication(AuthenticationContext filterContext)
        {
            var allowAnonymousAction = DoesActionAttributeDefined<AllowAnonymousAttribute>(filterContext)
                || (DoesControllerAttributeDefined<AllowAnonymousAttribute>(filterContext)
                && !DoesActionAttributeDefined<AuthenticationAttribute>(filterContext));

            var cookie = CookiesHelper.Get(CookiesHelper.CookieKey.Auth,
                filterContext.RequestContext.HttpContext.Request.Cookies);

            var isLoginPage = filterContext.ActionDescriptor.ActionName == "Login"
                && filterContext.ActionDescriptor.ControllerDescriptor.ControllerName == "Account";
            var isLogoutPage = filterContext.ActionDescriptor.ActionName == "Logout"
                && filterContext.ActionDescriptor.ControllerDescriptor.ControllerName == "Account";

            if (CookiesHelper.Validate<Token>(cookie))
            {
                if (isLoginPage)
                    filterContext.Result = new HttpStatusCodeResult(HttpStatusCode.Moved);
                else if (!String.IsNullOrWhiteSpace(Role))
                {
                    var token = CookiesHelper.GetCookieValue<Token>(cookie);

                    if (token != null && !String.Equals(token.Ur, Role, StringComparison.InvariantCultureIgnoreCase))
                    {
                        //make another logic here
                        CookiesHelper.Delete(CookiesHelper.CookieKey.Auth, filterContext.RequestContext.HttpContext.Response);
                        filterContext.Result = new HttpUnauthorizedResult();
                    }
                    else
                        filterContext.Controller.TempData["User"] = token.Ul;
                }
            }
            else if (isLogoutPage || !allowAnonymousAction)
                filterContext.Result = new HttpUnauthorizedResult();
        }

        public void OnAuthenticationChallenge(AuthenticationChallengeContext filterContext)
        {
            if (filterContext.Result == null)
                filterContext.Result = new HttpUnauthorizedResult();

            if (filterContext.Result is HttpUnauthorizedResult)
                RedirectToLogin(filterContext);
            else if (filterContext.Result is HttpStatusCodeResult)
            {
                var statusCodeResult = filterContext.Result as HttpStatusCodeResult;
                var statusCode = (HttpStatusCode)(statusCodeResult).StatusCode;

                if (statusCode == HttpStatusCode.Moved)
                    RedirectTo("/", String.Empty, filterContext);
            }
        }

        private void RedirectTo(String controller, String action, AuthenticationChallengeContext context)
        {
            context.RequestContext.HttpContext.Response.RedirectToRoutePermanent(
                        new System.Web.Routing.RouteValueDictionary{
                        {"controller", controller},
                        {"action", action}});
        }

        private void RedirectToLogin(AuthenticationChallengeContext context)
        {
            context.Result =
                new RedirectToRouteResult("Default", new System.Web.Routing.RouteValueDictionary{
                        {"controller", "Account"},
                        {"action", "Login"},
                        {"returnUrl", context.HttpContext.Request.RawUrl}});
        }

        private Boolean DoesActionAttributeDefined<T>(AuthenticationContext context) where T : Attribute
        {
            return context.ActionDescriptor.IsDefined(typeof(T), false);
        }

        private Boolean DoesControllerAttributeDefined<T>(AuthenticationContext context) where T : Attribute
        {
            return context.ActionDescriptor.ControllerDescriptor.IsDefined(typeof(T), false);
        }
    }
}