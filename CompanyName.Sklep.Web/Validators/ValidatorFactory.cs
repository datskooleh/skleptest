﻿using FluentValidation;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CompanyName.Sklep.Web.Validators
{
    public class ValidatorFactory : ValidatorFactoryBase
    {
        private readonly IUnityContainer _container;

        public ValidatorFactory(IUnityContainer container)
        {
            _container = container;
        }

        public override IValidator CreateInstance(Type validatorType)
        {
            try
            {
                return _container.Resolve(validatorType) as IValidator;
            }
            catch
            {
                return null;
            }
        }
    }
}