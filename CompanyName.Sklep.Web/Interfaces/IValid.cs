﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CompanyName.Sklep.Web.Interfaces
{
    internal interface ICookieObjectIsValid
    {
        Boolean IsValid();
    }
}
