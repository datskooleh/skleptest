﻿using CompanyName.Sklep.Services.Interfaces;
using CompanyName.Sklep.Services.Models;
using CompanyName.Sklep.Web.Filters;
using CompanyName.Sklep.Web.Helpers;
using FluentValidation;
using Microsoft.Practices.Unity;
using System;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace CompanyName.Sklep.Web.Controllers
{
    public class UserController : Controller
    {
        private IUserService _userService;
        private IRoleService _roleService;
        private IUnityContainer _container;

        public UserController(IUserService userService
            ,IRoleService roleService
            ,IUnityContainer container)
        {
            this._userService = userService;
            this._roleService = roleService;
            this._container = container;
        }

        [HttpGet]
        public ActionResult Index(String message)
        {
            UsersListViewModel model = null;

            model = _userService.GetAll();

            if (!String.IsNullOrWhiteSpace(message))
                ModelState.AddModelError("Message", message);

            return View(model);
        }

        [HttpGet]
        public ActionResult Edit(Int32? id)
        {
            ActionResult view;

            if (id.HasValue && id.Value > 0)
            {
                var model = _userService.GetForEdit(id.Value);

                if (model == null)
                    view = RedirectToAction("Index");
                else
                    view = View(model);
            }
            else
                view = RedirectToAction("Index");

            return view;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(EditUserViewModel user)
        {
            ActionResult view;

            user.Roles = _roleService.GetAll().ToList();
            if (ModelState.IsValid && _userService.Update(user))
                view = RedirectToAction("Index");
            else
            {
                if (!String.IsNullOrWhiteSpace(user.Message))
                    ModelState.AddModelError("Message", user.Message);

                view = View(user);
            }

            return view;
        }

        [HttpGet]
        public ActionResult Create()
        {
            var model = new CreateUserViewModel();
            model.Roles = _roleService.GetAll();

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CreateUserViewModel user)
        {
            ActionResult view;

            user.Roles = _roleService.GetAll().ToList();
            if (ModelState.IsValid && _userService.Save(user))
                view = RedirectToAction("Index");
            else
            {
                if (!String.IsNullOrWhiteSpace(user.Message))
                    ModelState.AddModelError("Message", user.Message);
                view = View(user);
            }

            return view;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(Int32? id)
        {
            ActionResult view;

            var validator = _container.Resolve<IValidator<Int32?>>();
            var result = validator.Validate(id);

            if (result.IsValid)
            {
                _userService.Delete(id.Value);
                view = RedirectToAction("Index");
            }
            else
                view = RedirectToAction("Index", new { message = result.Errors.First().ErrorMessage });


            return view;
        }
    }
}