﻿using CompanyName.Sklep.Services.Interfaces;
using CompanyName.Sklep.Services.Models;
using CompanyName.Sklep.Web.Interfaces;
using CompanyName.Sklep.Web.Helpers;
using Newtonsoft.Json;
using System;
using System.Web.Mvc;
using System.Web.Security;

namespace CompanyName.Sklep.Web.Controllers
{
    public class AccountController : Controller
    {
        private IAccountService _accountService = null;

        public AccountController()
        {
            var context = new DataAccess.EntityFramework.AppDbContext();

            this._accountService = new CompanyName.Sklep.Services.Services.AccountService();
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult Login(String returnUrl = null)
        {
            LoginViewModel model = new LoginViewModel();
            model.ReturnUrl = returnUrl;

            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel model)
        {
            ActionResult view;

            if (ModelState.IsValid)
            {
                var user = _accountService.GetByLogin(model.Login);

                var token = new Token()
                {
                    At = Guid.NewGuid().ToString().Replace("-", ""),
                    CtE = DateTime.UtcNow.AddSeconds(86400),
                    Ul = user.Login,
                    Ur = _accountService.GetRole(model.Login)
                };
                CookiesHelper.Set(CookiesHelper.CookieKey.Auth, token, Response);

                if (String.IsNullOrWhiteSpace(model.ReturnUrl))
                    view = RedirectToAction("Index", "Article");
                else
                    view = Redirect(model.ReturnUrl);
            }
            else
                view = View(model);

            return view;
        }

        [HttpPost]
        public ActionResult Logout()
        {
            CookiesHelper.Delete(CookiesHelper.CookieKey.Auth, Response);

            return RedirectToAction("Login");
        }

        [HttpGet]
        public ActionResult GetLogin()
        {
            var cookie = CookiesHelper.GetCookieValue<Token>(
                CookiesHelper.Get(CookiesHelper.CookieKey.Auth, Request.Cookies));

            return Content(cookie != null
                ? cookie.Ul
                : String.Empty);
        }
    }
}