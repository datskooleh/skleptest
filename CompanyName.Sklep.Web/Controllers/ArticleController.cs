﻿using CompanyName.Sklep.Services.Interfaces;
using CompanyName.Sklep.Services.Models;
using CompanyName.Sklep.Web.Filters;
using System;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace CompanyName.Sklep.Web.Controllers
{
    public class ArticleController : Controller
    {
        private IArticleService _articleService = null;

        public ArticleController(IArticleService articleService)
        {
            this._articleService = articleService;
        }

        [HttpGet]
        public ActionResult Index()
        {
            ArticlesListViewModel model = null;

            model = _articleService.GetAll();

            return View(model);
        }

        [HttpGet]
        public ActionResult Create(Boolean isModal = false)
        {
            if (isModal)
                return PartialView();
            else
                return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CreateArticleViewModel article)
        {
            ActionResult view;

            if (ModelState.IsValid)
            {
                if (_articleService.Save(article))
                    view = RedirectToAction("Index");
                else
                    view = View(article);
            }
            else
                view = View(article);

            return view;
        }

        [HttpGet]
        public ActionResult EditPrice(Int32? id)
        {
            EditArticlePriceViewModel model = null;

            ActionResult view;
            if (id.HasValue)
            {
                model = _articleService.GetForEdit(id.Value);

                if (model == null)
                    view = HttpNotFound("Item does not exist");
                else
                    view = View(model);
            }
            else
                view = new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            return view;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditPrice(EditArticlePriceViewModel article)
        {
            ActionResult view;

            if (ModelState.IsValid)
            {
                //if (_articleService.UpdatePrice(article))
                //    view = new HttpStatusCodeResult(HttpStatusCode.OK);
                //else
                //    view = new HttpStatusCodeResult(HttpStatusCode.NotModified);
                _articleService.UpdatePrice(article);
                view = RedirectToAction("Index");
            }
            else
                view = View(article);

            return view;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(Int32? id)
        {
            ActionResult view;

            if (id.HasValue && id > 0)
            {
                _articleService.Delete(id.Value);
                view = RedirectToAction("Index");
            }
            else
                view = new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            return view;
        }
    }
}