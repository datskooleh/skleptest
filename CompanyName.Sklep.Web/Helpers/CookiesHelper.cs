﻿using CompanyName.Sklep.Common.Interfaces;
using CompanyName.Sklep.Services.Interfaces;
using CompanyName.Sklep.Web.App_Start;
using CompanyName.Sklep.Web.Interfaces;
using Microsoft.Practices.Unity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CompanyName.Sklep.Web.Helpers
{
    internal static class CookiesHelper
    {
        public enum CookieKey
        {
            Auth
        }

        private static readonly String AuthenticationTokenKey = "AC";

        /// <summary>
        /// Get cookie from collection
        /// </summary>
        /// <param name="key">Cookie type to search</param>
        /// <param name="collection">Cookies collection for search</param>
        /// <returns>Requested cookie or NULL if it does not exists</returns>
        public static HttpCookie Get(CookieKey key, HttpCookieCollection collection)
        {
            HttpCookie cookie = null;

            String keyToSearch = KeyToString(key);

            if (collection.AllKeys.ToList().Any(x => x == keyToSearch))
                cookie = collection[keyToSearch];

            return cookie;
        }

        /// <summary>
        /// Get cookie value
        /// </summary>
        /// <typeparam name="T">Cookie value type</typeparam>
        /// <param name="key">Cookie type to search</param>
        /// <param name="collection">Cookies collection for search</param>
        /// <returns>Return deciphered cookie value or NULL if error</returns>
        public static T GetCookieValue<T>(CookieKey key, HttpCookieCollection collection) where T : ICookieObjectIsValid
        {
            T cookieValue;

            var cookie = Get(key, collection);
            cookieValue = GetCookieValue<T>(cookie);

            return cookieValue;
        }

        /// <summary>
        /// Get cookie value
        /// </summary>
        /// <typeparam name="T">Cookie value type</typeparam>
        /// <param name="cookie">Cookie to get value from</param>
        /// <returns>Return deciphered cookie value or NULL if error</returns>
        public static T GetCookieValue<T>(HttpCookie cookie) where T : ICookieObjectIsValid
        {
            T cookieValue;

            try
            {
                var decoded = DecodeCookieValue(cookie.Value);
                cookieValue = JsonConvert.DeserializeObject<T>(decoded);
            }
            catch
            {
                cookieValue = default(T);
            }

            return cookieValue;
        }

        /// <summary>
        /// Add new cookie with the provided value to the response
        /// </summary>
        /// <typeparam name="T">Type of object to be jsoned</typeparam>
        /// <param name="key">Cookie type to search</param>
        /// <param name="obj">Cookie value</param>
        /// <param name="response">Response that will contain result cookie</param>
        public static void Set<T>(CookieKey key, T obj, HttpResponseBase response) where T : ICookieObjectIsValid
        {
            var cookie = new HttpCookie(KeyToString(key));

            if (cookie != null)
            {
                var json = JsonConvert.SerializeObject(obj);

                cookie.Value = EncodeCookieValue(json);
                cookie.HttpOnly = true;
                cookie.Expires = DateTime.Now.AddSeconds(86400).ToUniversalTime(); //one day

                response.Cookies.Add(cookie);
            }
        }

        /// <summary>
        /// Delete cookie from client
        /// </summary>
        /// <param name="key">Cookie key to delete</param>
        /// <param name="response">Response where to write cookie</param>
        public static void Delete(CookieKey key, HttpResponseBase response)
        {
            var cookie = new HttpCookie(KeyToString(key));

            if (cookie != null)
            {
                cookie.Value = "";
                cookie.HttpOnly = true;
                cookie.Expires = DateTime.UtcNow;

                response.Cookies.Add(cookie);
            }
        }

        /// <summary>
        /// Validate cookie using special object interface
        /// </summary>
        /// <typeparam name="T">Cookie object that saved as value in cookie</typeparam>
        /// <param name="cookie"></param>
        /// <returns>True if cookie is valid</returns>
        public static Boolean Validate<T>(HttpCookie cookie) where T : ICookieObjectIsValid
        {
            var isValid = false;

            if (cookie != null && !String.IsNullOrWhiteSpace(cookie.Value))
                isValid = DateTime.Now.Subtract(cookie.Expires.ToLocalTime()).TotalMilliseconds > 0;

            if (isValid)
            {
                Token value = GetCookieValue<Token>(cookie);

                if (value != null)
                    isValid = value.IsValid();
                else
                    isValid = false;
            }

            return isValid;
        }

        private static String KeyToString(CookieKey key)
        {
            String stringKey = String.Empty;

            switch (key)
            {
                case CookieKey.Auth:
                    stringKey = CookiesHelper.AuthenticationTokenKey;
                    break;
                default:
                    throw new InvalidOperationException("Unsupported type cookie key");
            }

            return stringKey;
        }

        private static String DecodeCookieValue(String str)
        {
            return ResolveType<ITwoWayHash>().Decode(str);
        }

        private static String EncodeCookieValue(String str)
        {
            return ResolveType<ITwoWayHash>().Encode(str);
        }

        private static T ResolveType<T>()
        {
            IUnityContainer container = UnityConfig.GetConfiguredContainer();
            return container.Resolve<T>();
        }
    }
}