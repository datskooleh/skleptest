﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Odbc;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace CompanyName.Sklep.DataAccess.AdoNet
{
    internal static class ConnectionHelper
    {
        public static String GetConnectionString()
        {
            String connectionString;

            var file = Directory.GetParent(EntryAssemblyDirectory).GetFiles()
                .SingleOrDefault(x => x.Name == "Web.config");

            if (file.Exists)
                connectionString = ParseFile(file.FullName);
            else
                throw new InvalidDataException("Config file not found");

            return connectionString;
        }

        private static String ParseFile(String filePath)
        {
            DbConnectionStringBuilder connectionStringBuilder = null;

            var file = new System.Xml.XmlDocument();
            file.Load(filePath);

            var connectionStringsTag = file.GetElementsByTagName("connectionStrings");

            if (connectionStringsTag.Count > 0)
            {
                var foundedAddElement = connectionStringsTag.Item(0).FirstChild;

                var attributes = new Dictionary<String, String>();

                foreach (XmlAttribute attribute in foundedAddElement.Attributes)
                    attributes.Add(attribute.Name, attribute.Value);

                try
                {
                    String connectionStringValue;
                    if (!attributes.TryGetValue("connectionString", out connectionStringValue))
                        throw new InvalidDataException("Connection string was not found");

                    if (attributes.ContainsKey("providerName"))
                    {
                        var provider = attributes["providerName"];

                        if (provider == "System.Data.SqlClient")
                            connectionStringBuilder = new SqlConnectionStringBuilder(connectionStringValue);
                        else if (provider == "System.Data.OleDb")
                            connectionStringBuilder = new OleDbConnectionStringBuilder(connectionStringValue);
                        else if (provider == "System.Data.Odbc")
                            connectionStringBuilder = new OdbcConnectionStringBuilder(connectionStringValue);
                        else if (provider == "System.Data.OracleClient")
                            throw new InvalidDataException("Provider is unsupported");
                    }
                    else
                        throw new InvalidDataException("Provider attribute was not found");
                }
                catch
                {
                    throw new InvalidDataException("Can't use current connection string to connect to db");
                }
            }

            return connectionStringBuilder.ConnectionString;
        }

        private static String EntryAssemblyDirectory
        {
            get
            {
                String codeBase = Assembly.GetExecutingAssembly().CodeBase;
                UriBuilder uri = new UriBuilder(codeBase);
                String path = Uri.UnescapeDataString(uri.Path);
                return Path.GetDirectoryName(path);
            }
        }
    }
}
