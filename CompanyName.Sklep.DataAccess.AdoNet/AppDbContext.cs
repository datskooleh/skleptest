﻿using CompanyName.Sklep.Common.Interfaces;
using CompanyName.Sklep.DataAccess.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace CompanyName.Sklep.DataAccess.AdoNet
{
    public class AppDbContext : IDataAccessContext
    {
        private String _connectionString;
        private SqlConnection _connection;

        private IMapperMiddleware _mapper;
        public AppDbContext(IMapperMiddleware mapper)
        {
            throw new NotImplementedException("Didn't have time to do this");

            this._mapper = mapper;
            this._connectionString = ConnectionHelper.GetConnectionString();

            try
            {
            }
            catch
            {
                throw new OperationCanceledException("Database can't be accessed");
            }

        }

        public IEnumerable<T2> GetAll<T1, T2>()
            where T1 : class, IEntity
            where T2 : class, IDTO
        {
            throw new NotImplementedException();
        }

        public IEnumerable<T2> GetAll<T1, T2>(Func<T2, Boolean> func)
            where T1 : class, IEntity
            where T2 : class, IDTO
        {
            throw new NotImplementedException();
        }

        public T2 Find<T1, T2>(Int32 id)
            where T1 : class, IEntity
            where T2 : class, IDTO
        {
            throw new NotImplementedException();
        }

        public Boolean Update<T1, T2>(T2 model)
            where T1 : class, IEntity
            where T2 : class, IDTO
        {
            throw new NotImplementedException();
        }

        public Boolean Save<T1, T2>(T2 model)
            where T1 : class, IEntity
            where T2 : class, IDTO
        {
            throw new NotImplementedException();
        }

        public Boolean Delete<T1, T2>(T2 model)
            where T1 : class, IEntity
            where T2 : class, IDTO
        {
            throw new NotImplementedException();
        }

        public bool Delete<T>(Int32 id) where T : class, IEntity
        {
            throw new NotImplementedException();
        }
    }
}
