﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Billenium.Sklep.Services.Interfaces;
using Billenium.Sklep.Services.Models;
using Billenium.Sklep.Services.Services;

namespace Billenium.Sklep.Tests
{
    [TestClass]
    public class AccountTests
    {
        IAccountService _accountService;

        public AccountTests(IAccountService accountService)
        {
            this._accountService = accountService;
        }

        [TestMethod]
        public void LoginFailed()
        {
            var data = new LoginViewModel()
            {
                Login = null,
                Password = null
            };
            Assert.IsFalse(_accountService.IsCredentialsValid(data));

            data.Password = "";
            Assert.IsFalse(_accountService.IsCredentialsValid(data));

            data.Login = "";
            data.Password = null;
            Assert.IsFalse(_accountService.IsCredentialsValid(data));

            data.Login = "";
            data.Password = "";
            Assert.IsFalse(_accountService.IsCredentialsValid(data));

            data.Login = "test";
            data.Password = "test";
            Assert.IsFalse(_accountService.IsCredentialsValid(data));
        }

        [TestMethod]
        public void LoginOk()
        {
            var data = new LoginViewModel()
            {
                Login = "admin",
                Password = "admin"
            };
            Assert.IsTrue(_accountService.IsCredentialsValid(data));
        }

        [TestMethod]
        public void GetAccountByIdFailed()
        {
            var id = -5;
            Assert.IsNull(_accountService.GetById(id));

            id = 700;
            Assert.IsNull(_accountService.GetById(id));
        }

        [TestMethod]
        public void GetAccountByIdOk()
        {
            var id = 2;
            Assert.IsNotNull(_accountService.GetById(id));
        }

        [TestMethod]
        public void GetAccountByLoginFailed()
        {
            String login = null;
            Assert.IsNull(_accountService.GetByLogin(login));

            login = "";
            Assert.IsNull(_accountService.GetByLogin(login));

            login = "test";
            Assert.IsNull(_accountService.GetByLogin(login));


        }

        [TestMethod]
        public void GetAccountbyLoginOk()
        {
            String login = "admin";
            Assert.IsNotNull(_accountService.GetByLogin(login));
        }
    }
}
