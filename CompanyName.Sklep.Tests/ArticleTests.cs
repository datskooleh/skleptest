﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Billenium.Sklep.Services.Interfaces;
using Billenium.Sklep.Services.Models;
using Billenium.Sklep.Services.Services;

namespace Billenium.Sklep.Tests
{
    [TestClass]
    public class ArticleTests
    {
        IArticleService _articleService;

        public ArticleTests(IArticleService articleService)
        {
            this._articleService = articleService;
        }

        [TestMethod]
        public void GetAll()
        {
            Assert.IsTrue(_articleService.Get().Items.Count() > 0);

            _articleService.Get().Items.ToList().RemoveAll(x => x.Id > 0);

            Assert.IsTrue(_articleService.Get().Items.Count() == 0);
        }

        [TestMethod]
        public void GetSingleFailed()
        {
            var id = -5;
            Assert.IsNull(_articleService.Get(id));

            id = 500;
            Assert.IsNull(_articleService.Get(id));
        }

        [TestMethod]
        public void GetSingleOk()
        {
            var id = 2;
            Assert.IsNotNull(_articleService.Get(id));
        }

        [TestMethod]
        public void GetForEditFailed()
        {
            Int32 id = -5;
            Assert.IsNull(_articleService.GetForEdit(id));

            id = 5;
            Assert.IsNull(_articleService.GetForEdit(id));


        }

        [TestMethod]
        public void GetForEditOk()
        {
            var id = _articleService.Get().Items.DefaultIfEmpty(new ArticleViewModel()).First().Id;
            Assert.IsNotNull(_articleService.GetForEdit(id));
        }

        [TestMethod]
        public void SaveFailed()
        {
            var count = _articleService.Get().Items.Count();

            var model = new CreateArticleViewModel()
            {
                Description = null,
                Name = null,
                Price = -5
            };
            Assert.AreEqual(_articleService.Save(model), -1);

            model.Name = "Test article name";
            Assert.AreEqual(_articleService.Save(model), -1);

            model.Name = "";
            model.Price = 5;
            Assert.AreEqual(_articleService.Save(model), -1);
        }

        [TestMethod]
        public void SaveOk()
        {
            var model = new CreateArticleViewModel()
            {
                Description = "Test description",
                Name = "Test name",
                Price = 5
            };

            Assert.AreNotEqual(_articleService.Save(model), -1);
        }

        [TestMethod]
        public void UpdateFailed()
        {
            EditArticlePriceViewModel articleForUpdate =
                _articleService.GetForEdit(_articleService
                .Get().Items
                .DefaultIfEmpty(new ArticleViewModel()).First().Id);

            if (articleForUpdate != null)
            {
                articleForUpdate.Price = -5;
                Assert.IsFalse(_articleService.UpdatePrice(articleForUpdate));
            }
        }

        [TestMethod]
        public void UpdateOk()
        {
            EditArticlePriceViewModel articleForUpdate =
                _articleService.GetForEdit(_articleService
                .Get().Items
                .DefaultIfEmpty(new ArticleViewModel()).First().Id);

            articleForUpdate.Price = articleForUpdate.Price * 2;
            Assert.IsTrue(_articleService.UpdatePrice(articleForUpdate));
        }

        [TestMethod]
        public void DeleteByIdFailed()
        {
            var id = -5;
            Assert.IsFalse(_articleService.Delete(id));

            id = 500;
            Assert.IsFalse(_articleService.Delete(id));
        }

        [TestMethod]
        public void DeleteByIdOk()
        {
            var id = _articleService.Get().Items.DefaultIfEmpty(new ArticleViewModel()).First().Id;
            Assert.IsTrue(_articleService.Delete(id));
        }

        [TestMethod]
        public void DeleteByModelFailed()
        {
            ArticleViewModel model = null;
            Assert.IsFalse(_articleService.Delete(model));

            model = new ArticleViewModel();
            Assert.IsFalse(_articleService.Delete(model));

            model.Id = -5;
            Assert.IsFalse(_articleService.Delete(model));
        }

        [TestMethod]
        public void DeleByModelOk()
        {
            ArticleViewModel model = _articleService.Get().Items.DefaultIfEmpty(new ArticleViewModel()).First();
            Assert.IsTrue(_articleService.Delete(model));
        }
    }
}
