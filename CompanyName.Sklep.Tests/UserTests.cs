﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Billenium.Sklep.Services.Interfaces;
using Billenium.Sklep.Services.Models;
using Billenium.Sklep.Services.Services;

namespace Billenium.Sklep.Tests
{
    [TestClass]
    public class UserTests
    {
        IUserService _userService;

        public UserTests(IUserService userService)
        {
            this._userService = userService;
        }

        [TestMethod]
        public void GetAll()
        {
            Assert.IsTrue(_userService.Get().Items.Count() > 0);

            _userService.Get().Items.ToList().RemoveAll(x => x.Id > 0);

            Assert.IsTrue(_userService.Get().Items.Count() == 0);
        }

        [TestMethod]
        public void GetForEditByIdFailed()
        {
            var id = -5;
            Assert.IsNull(_userService.GetForEdit(id));

            id = 5000;
            Assert.IsNull(_userService.GetForEdit(id));
        }

        [TestMethod]
        public void GetForEditByIdOk()
        {
            var id = _userService.Get().Items.DefaultIfEmpty(new UserViewModel()).First().Id;
            Assert.IsNotNull(_userService.GetForEdit(id));
        }

        [TestMethod]
        public void GetForEditByModelFailed()
        {
            UserViewModel model = null;
            Assert.IsNull(_userService.GetForEdit(model));

            model = new UserViewModel();
            Assert.IsNull(_userService.GetForEdit(model));
        }

        [TestMethod]
        public void GetForEditByModelOk()
        {
            var model = _userService.Get().Items.DefaultIfEmpty(new UserViewModel()).First();
            Assert.IsNotNull(_userService.GetForEdit(model));
        }

        [TestMethod]
        public void SaveFailed()
        {
            var count = _userService.Get().Items.Count();

            CreateUserViewModel model = null;
            Assert.AreEqual(_userService.Save(model), -1);

            model = new CreateUserViewModel()
            {
                Login = null,
                Password = null
            };
            Assert.AreEqual(_userService.Save(model), -1);

            model.Login = "";
            Assert.AreEqual(_userService.Save(model), -1);

            model.Login = "TestUser";
            Assert.AreEqual(_userService.Save(model), -1);

            model.Login = null;
            model.Password = "";
            Assert.AreEqual(_userService.Save(model), -1);

            model.Password = "TestUser";
            Assert.AreEqual(_userService.Save(model), -1);
        }

        [TestMethod]
        public void SaveOk()
        {
            var model = new CreateUserViewModel()
            {
                Login = null,
                Password = null
            };

            model.Login = "TestUser";
            model.Password = "TestUser";
            var newId = _userService.Save(model);
            Assert.AreNotEqual(newId, -1);
            var updatedModel = _userService.Get().Items.SingleOrDefault(x => x.Login == model.Login);
            Assert.IsNotNull(updatedModel);
            Assert.AreEqual(updatedModel.Login, "TestUser");
        }

        [TestMethod]
        public void UpdateFailed()
        {
            EditUserViewModel modelToUpdate =
                _userService.GetForEdit(_userService
                .Get().Items
                .DefaultIfEmpty(new UserViewModel()).First().Id);

            modelToUpdate.FirstName = "Test Change";
            modelToUpdate.SelectedRole = modelToUpdate.Roles.Last().Id;
            modelToUpdate.LastName = "Test Change";

            Assert.IsFalse(_userService.Update(modelToUpdate));
        }

        [TestMethod]
        public void UpdateOk()
        {
            Int32 roleId;
            String newFirsLastName = "Test Change";

            EditUserViewModel modelToUpdate =
                _userService.GetForEdit(_userService
                .Get().Items
                .DefaultIfEmpty(new UserViewModel()).First().Id);

            roleId = modelToUpdate.Roles
                    .FirstOrDefault(x => x.Id != ((modelToUpdate.SelectedRole == null)
                        ? 0
                        : modelToUpdate.SelectedRole)).Id;

            modelToUpdate.SelectedRole = modelToUpdate.Roles.SingleOrDefault(x => x.Id == roleId).Id;
            modelToUpdate.LastName = newFirsLastName;
            modelToUpdate.FirstName = newFirsLastName;
            Assert.IsTrue(_userService.Update(modelToUpdate));

            var newModel = _userService.Get().Items.SingleOrDefault(x => x.Id == modelToUpdate.Id);
            Assert.AreEqual(newModel.FirstName, newFirsLastName);
            Assert.AreEqual(newModel.LastName, newFirsLastName);
            Assert.AreEqual(newModel.Role, roleId);
        }

        [TestMethod]
        public void DeleteByIdFailed()
        {
            var id = -5;
            Assert.IsFalse(_userService.Delete(id));

            id = 500;
            Assert.IsFalse(_userService.Delete(id));
        }

        [TestMethod]
        public void DeleteByIdOk()
        {
            var id = _userService.Get().Items.DefaultIfEmpty(new UserViewModel()).First().Id;
            Assert.IsTrue(_userService.Delete(id));
            Assert.IsNull(_userService.GetForEdit(id));
        }

        [TestMethod]
        public void DeleteByModelFailed()
        {
            UserViewModel model = null;
            Assert.IsFalse(_userService.Delete(model));

            model = new UserViewModel();
            Assert.IsFalse(_userService.Delete(model));

            model.Id = -5;
            Assert.IsFalse(_userService.Delete(model));
        }

        [TestMethod]
        public void DeleteByModelOk()
        {
            UserViewModel model = _userService.Get().Items.DefaultIfEmpty(new UserViewModel()).First();
            Assert.IsTrue(_userService.Delete(model));
            Assert.IsNull(_userService.GetForEdit(model.Id));
        }
    }
}
