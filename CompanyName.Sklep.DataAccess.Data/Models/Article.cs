﻿using CompanyName.Sklep.DataAccess.Data.Interfaces;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CompanyName.Sklep.DataAccess.Data.Models
{
    [Table("Aticle")]
    public class Article : IEntity
    {
        [Key]
        [Column("ArticleId")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int32 Id { get; set; }

        [Required]
        [Column("Name")]
        [StringLength(50, MinimumLength=2)]
        public String Name { get; set; }

        [Column("Description")]
        [StringLength(200)]
        public String Description { get; set; }

        [Required]
        [Column("Count")]
        public Int32 AvailableCount { get; set; }

        [Required]
        [Column("Price")]
        public Decimal Price { get; set; }
    }
}
