﻿using CompanyName.Sklep.DataAccess.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CompanyName.Sklep.DataAccess.Data.Models
{
    [Table("Role")]
    public class Role : IEntity
    {
        public Role()
        {
            Users = new List<User>();
        }

        [Key]
        [Column("RoleId")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int32 Id { get; set; }

        [Required]
        [Column("Name")]
        public String Name { get; set; }

        public virtual ICollection<User> Users { get; set; }
    }
}
