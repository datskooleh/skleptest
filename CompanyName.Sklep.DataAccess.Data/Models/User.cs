﻿using CompanyName.Sklep.DataAccess.Data.Interfaces;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CompanyName.Sklep.DataAccess.Data.Models
{
    [Table("User")]
    public class User : IEntity
    {
        public User()
        {
            DateRegistered = DateTime.Now;
            LastLogin = null;
        }

        [Key]
        [Column("UserId")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int32 Id { get; set; }

        [Required]
        [Column("Login")]
        [StringLength(25)]
        public String Login { get; set; }

        [Required]
        [Column("Password")]
        [StringLength(128)]
        public String Password { get; set; }

        [Column("FirstName")]
        [StringLength(25)]
        public String FirstName { get; set; }

        [Column("LastName")]
        [StringLength(40)]
        public String LastName { get; set; }

        [Required]
        [Column("Country")]
        [StringLength(25)]
        public String Country { get; set; }

        [Column("City")]
        [StringLength(40)]
        public String City { get; set; }

        [Column("AddressLine1")]
        [StringLength(100)]
        public String AddressLine1 { get; set; }

        [Column("AddressLine2")]
        [StringLength(100)]
        public String AddressLine2 { get; set; }

        [Column("Created")]
        public DateTime DateRegistered { get; set; }

        [Column("LastLogin")]
        public DateTime? LastLogin { get; set; }

        [Required]
        [Column("Role")]
        public Int32 RoleId { get; set; }

        [ForeignKey("RoleId")]
        public virtual Role Role { get; set; }
    }
}
