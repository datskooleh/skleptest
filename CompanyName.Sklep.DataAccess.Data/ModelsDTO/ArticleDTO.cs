﻿using CompanyName.Sklep.Common.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompanyName.Sklep.DataAccess.Data.ModelsDTO
{
    public class ArticleDTO : IDTO
    {
        public Int32 Id { get; set; }

        public String Name { get; set; }

        public String Description { get; set; }

        public Int32 AvailableCount { get; set; }

        public Decimal Price { get; set; }
    }
}
