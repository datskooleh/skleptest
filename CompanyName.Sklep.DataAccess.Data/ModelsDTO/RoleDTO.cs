﻿using CompanyName.Sklep.Common.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompanyName.Sklep.DataAccess.Data.ModelsDTO
{
    public class RoleDTO : IDTO
    {
        public RoleDTO()
        {
            Users = new List<UserDTO>();
        }

        public Int32 Id { get; set; }

        public String Name { get; set; }

        public ICollection<UserDTO> Users { get; set; }
    }
}
