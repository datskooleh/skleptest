﻿using CompanyName.Sklep.Common.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompanyName.Sklep.DataAccess.Data.ModelsDTO
{
    public class UserDTO : IDTO
    {
        public Int32 Id { get; set; }

        public String Login { get; set; }

        public String Password { get; set; }

        public String FirstName { get; set; }

        public String LastName { get; set; }

        public String Country { get; set; }

        public String City { get; set; }

        public String AddressLine1 { get; set; }

        public String AddressLine2 { get; set; }

        public DateTime DateRegistered { get; set; }

        public DateTime? LastLogin { get; set; }

        public RoleDTO Role { get; set; }
    }
}
