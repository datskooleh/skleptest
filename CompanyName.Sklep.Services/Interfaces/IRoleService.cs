﻿using CompanyName.Sklep.Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompanyName.Sklep.Services.Interfaces
{
    public interface IRoleService
    {
        IEnumerable<RoleViewModel> GetAll();

        RoleViewModel Get(Int32 id);

        Boolean Exist(Int32 id);

        Boolean Exist(String name);
    }
}
