﻿using CompanyName.Sklep.Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompanyName.Sklep.Services.Interfaces
{
    public interface IArticleService
    {
        /// <summary>
        /// Get all items in system
        /// </summary>
        /// <returns>All existing articles</returns>
        ArticlesListViewModel GetAll();

        /// <summary>
        /// Get specific article
        /// </summary>
        /// <param name="id">Article id</param>
        /// <returns>Article data if exist or null otherwise</returns>
        ArticleViewModel Get(Int32 id);

        /// <summary>
        /// Get article for editing
        /// </summary>
        /// <param name="id">Article id</param>
        /// <returns>Return not null if specifiedarticle exist</returns>
        EditArticlePriceViewModel GetForEdit(Int32 id);

        /// <summary>
        /// Add new article
        /// </summary>
        /// <param name="data">Data article to be saved</param>
        /// <returns>Return true if model was saved</returns>
        Boolean Save(CreateArticleViewModel data);

        /// <summary>
        /// Update exist article
        /// </summary>
        /// <param name="data">Data to be updated</param>
        /// <returns>Return true if price is updated and false if error occured</returns>
        Boolean UpdatePrice(EditArticlePriceViewModel data);

        /// <summary>
        /// Remove article from the system
        /// </summary>
        /// <param name="id">Article id to be removed</param>
        /// <returns>True if article was removed, false otherwise</returns>
        Boolean Delete(Int32 id);

        /// <summary>
        /// Remove article from the system
        /// </summary>
        /// <param name="data">Data of existing article</param>
        /// <returns>True if article was removed or false otherwise</returns>
        Boolean Delete(ArticleViewModel data);
    }
}
