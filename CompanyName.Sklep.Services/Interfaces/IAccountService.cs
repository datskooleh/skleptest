﻿using CompanyName.Sklep.Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompanyName.Sklep.Services.Interfaces
{
    public interface IAccountService
    {
        /// <summary>
        /// Check if user is registered in database
        /// </summary>
        /// <param name="data">User data to check</param>
        /// <returns>True if user is registered and provided values is valid</returns>
        Boolean IsCredentialsValid(LoginViewModel data);

        /// <summary>
        /// Get profile for the user
        /// </summary>
        /// <param name="id">User id</param>
        /// <returns>Profile details or null if not found</returns>
        ProfileDetailsViewModel GetById(Int32 id);

        /// <summary>
        /// Get profile for the user
        /// </summary>
        /// <param name="login">User login</param>
        /// <returns>Profile details or null if not found</returns>
        ProfileDetailsViewModel GetByLogin(String login);


        /// <summary>
        /// Get user role by it's login
        /// </summary>
        /// <param name="user">User login</param>
        /// <returns>Return role in string or null if not valid</returns>
        String GetRole(String login);
    }
}
