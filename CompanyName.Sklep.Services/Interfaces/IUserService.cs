﻿using CompanyName.Sklep.Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompanyName.Sklep.Services.Interfaces
{
    public interface IUserService
    {
        /// <summary>
        /// Get all items in system
        /// </summary>
        /// <returns>All existing users</returns>
        UsersListViewModel GetAll();

        /// <summary>
        /// Get specific user
        /// </summary>
        /// <param name="id">User id</param>
        /// <returns>User data if exist or null otherwise</returns>
        UserViewModel Get(Int32 id);

        /// <summary>
        /// Get user data available for update
        /// </summary>
        /// <param name="id">User id to search for</param>
        /// <returns>User mode for editing</returns>
        EditUserViewModel GetForEdit(Int32 id);

        /// <summary>
        /// Get user data available for update
        /// </summary>
        /// <param name="data">User model to be updated</param>
        /// <returns>User mode for editing</returns>
        EditUserViewModel GetForEdit(UserViewModel data);

        /// <summary>
        /// Update existing user
        /// </summary>
        /// <param name="data">New user data to be saved</param>
        /// <returns>Return true if updated and false if error occured</returns>
        Boolean Update(EditUserViewModel data);

        /// <summary>
        /// Add new user
        /// </summary>
        /// <param name="data">User data to be saved</param>
        /// <returns>return true if model was saved</returns>
        Boolean Save(CreateUserViewModel data);

        /// <summary>
        /// Remove user from the system
        /// </summary>
        /// <param name="id">User id to be removed</param>
        /// <returns>True if user was removed or false otherwise</returns>
        Boolean Delete(Int32 id);

        /// <summary>
        /// Remove user from the system
        /// </summary>
        /// <param name="data">User model to be removed</param>
        /// <returns>True if user was removed or false otherwise</returns>
        Boolean Delete(UserViewModel data);
    }
}
