﻿using CompanyName.Sklep.Services.Validators;
using FluentValidation.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompanyName.Sklep.Services.Models
{
    [Validator(typeof(EditArticlePriceValidator))]
    public class EditArticlePriceViewModel
    {
        public Int32 Id { get; set; }

        public String Name { get; set; }

        [DisplayName("New Price")]
        public Decimal Price { get; set; }

        [DisplayName("Current Price")]
        public Decimal OldPrice { get; set; }
    }
}
