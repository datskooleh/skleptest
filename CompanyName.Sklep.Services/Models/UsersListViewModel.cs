﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompanyName.Sklep.Services.Models
{
    public class UsersListViewModel
    {
        public IEnumerable<UserViewModel> Items { get; set; }

        public String Message { get; set; }
    }
}
