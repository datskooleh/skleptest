﻿using CompanyName.Sklep.Services.Validators;
using FluentValidation.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompanyName.Sklep.Services.Models
{
    [Validator(typeof(LoginValidator))]
    public class LoginViewModel
    {
        public String Login { get; set; }

        public String Password { get; set; }

        public String ReturnUrl { get; set; }

        public String Message { get; set; }
    }
}
