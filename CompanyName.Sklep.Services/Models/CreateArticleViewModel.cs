﻿using CompanyName.Sklep.Services.Validators;
using FluentValidation.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompanyName.Sklep.Services.Models
{
    [Validator(typeof(CreateArticleValidator))]
    public class CreateArticleViewModel
    {
        public String Name { get; set; }

        public String Description { get; set; }

        public Decimal Price { get; set; }

        [DisplayName("Count")]
        public Int32 AvailableCount { get; set; }
    }
}
