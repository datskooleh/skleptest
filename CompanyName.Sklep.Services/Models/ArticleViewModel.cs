﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompanyName.Sklep.Services.Models
{
    public class ArticleViewModel
    {
        public Int32 Id { get; set; }

        public String Name { get; set; }

        public String Description { get; set; }

        [DisplayName("Count")]
        public Int32 AvailableCount { get; set; }

        public Decimal Price { get; set; }
    }
}
