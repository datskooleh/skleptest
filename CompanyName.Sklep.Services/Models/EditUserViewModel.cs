﻿using CompanyName.Sklep.Services.Validators;
using FluentValidation.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompanyName.Sklep.Services.Models
{
    [Validator(typeof(EditUserValidator))]
    public class EditUserViewModel
    {
        public Int32 Id { get; set; }

        public String Login { get; set; }

        [DisplayName("First Name")]
        public String FirstName { get; set; }

        [DisplayName("Last Name")]
        public String LastName { get; set; }

        public String Country { get; set; }

        public String City { get; set; }

        [DisplayName("Address Line 1")]
        public String AddressLine1 { get; set; }

        [DisplayName("Address Line 2")]
        public String AddressLine2 { get; set; }

        [DisplayName("Role")]
        public Int32 SelectedRole { get; set; }

        public IList<RoleViewModel> Roles { get; set; }

        public String Message { get; set; }
    }
}
