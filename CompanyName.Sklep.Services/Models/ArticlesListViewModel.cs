﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompanyName.Sklep.Services.Models
{
    public class ArticlesListViewModel
    {
        public IEnumerable<ArticleViewModel> Items;
    }
}
