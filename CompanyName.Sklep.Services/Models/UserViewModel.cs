﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompanyName.Sklep.Services.Models
{
    public class UserViewModel
    {
        public Int32 Id { get; set; }

        public String Login { get; set; }

        [DisplayName("First Name")]
        public String FirstName { get; set; }

        [DisplayName("Last Name")]
        public String LastName { get; set; }

        public String Address { get; set; }

        public String Created { get; set; }

        [DisplayName("Last login")]
        public String LastLogin { get; set; }

        public String Role { get; set; }
    }
}
