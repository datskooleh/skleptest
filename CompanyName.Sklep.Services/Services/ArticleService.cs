﻿using CompanyName.Sklep.Common.Interfaces;
using CompanyName.Sklep.DataAccess.Data.Models;
using CompanyName.Sklep.DataAccess.Data.ModelsDTO;
using CompanyName.Sklep.Services.Interfaces;
using CompanyName.Sklep.Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompanyName.Sklep.Services.Services
{
    public class ArticleService : IArticleService
    {
        IDataAccessContext _context;
        IMapperMiddleware _mapper;

        public ArticleService(IDataAccessContext context, IMapperMiddleware mapper)
        {
            this._context = context;
            this._mapper = mapper;
        }

        public ArticlesListViewModel GetAll()
        {
            ArticlesListViewModel model = new ArticlesListViewModel()
            {
                Items = new List<ArticleViewModel>(),
            };

            var articles = _context.GetAll<Article, ArticleDTO>();

            model.Items = _mapper.Map<IEnumerable<ArticleDTO>, IEnumerable<ArticleViewModel>>(articles);

            return model;
        }

        public ArticlesListViewModel Filter(Int32? page, Int32 size = 10)
        {
            if (page.HasValue == false || page.Value < 0)
                page = 0;
            if (size < 10)
                size = 10;
            else if (size > 25)
                size = 25;

            var articles = _context.GetAll<Article, ArticleDTO>();

            ArticlesListViewModel model = new ArticlesListViewModel()
            {
                Items = new List<ArticleViewModel>(),
            };

            return model;
        }

        public ArticleViewModel Get(Int32 id)
        {
            ArticleViewModel model = null;

            var article = _context.Find<Article, ArticleDTO>(id);

            if (article != null)
                model = _mapper.Map<ArticleDTO, ArticleViewModel>(article);

            return model;
        }

        public Boolean Save(CreateArticleViewModel data)
        {
            var articleDTO = _mapper.Map<CreateArticleViewModel, ArticleDTO>(data);

            var saved = _context.Save<Article, ArticleDTO>(articleDTO);

            return saved;
        }

        public Boolean UpdatePrice(EditArticlePriceViewModel data)
        {
            var articleDTO = _context.Find<Article, ArticleDTO>(data.Id);

            var updated = false;

            if (articleDTO != null && articleDTO.Price != data.Price)
            {
                articleDTO.Price = data.Price;
                updated = _context.Update<Article, ArticleDTO>(articleDTO);
            }

            return updated;
        }

        public EditArticlePriceViewModel GetForEdit(Int32 id)
        {
            EditArticlePriceViewModel model = null;

            var article = _context.Find<Article, ArticleDTO>(id);

            if (article != null)
                model = _mapper.Map<ArticleDTO, EditArticlePriceViewModel>(article);

            return model;
        }

        public Boolean Delete(Int32 id)
        {
            return _context.Delete<Article>(id);
        }

        public Boolean Delete(ArticleViewModel data)
        {
            return this.Delete(data.Id);
        }
    }
}
