﻿using CompanyName.Sklep.Common.Interfaces;
using CompanyName.Sklep.DataAccess.Data.Models;
using CompanyName.Sklep.DataAccess.Data.ModelsDTO;
using CompanyName.Sklep.Services.Interfaces;
using CompanyName.Sklep.Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompanyName.Sklep.Services.Services
{
    public class RoleService : IRoleService
    {
        IDataAccessContext _context;
        IMapperMiddleware _mapper;

        public RoleService(IDataAccessContext context, IMapperMiddleware mapper)
        {
            this._context = context;
            this._mapper = mapper;
        }

        public IEnumerable<RoleViewModel> GetAll()
        {
            return _mapper.Map<List<RoleDTO>, List<RoleViewModel>>(_context.GetAll<Role, RoleDTO>().ToList());
        }

        public RoleViewModel Get(Int32 id)
        {
            var role = _context.Find<Role, RoleDTO>(id);

            RoleViewModel model = null;
            if (role != null)
                model = _mapper.Map<RoleDTO, RoleViewModel>(role);

            return model;
        }

        public Boolean Exist(Int32 id)
        {
            return _context.Find<Role, RoleDTO>(id) != null;
        }

        public Boolean Exist(String name)
        {
            var role = _context
                .GetAll<Role, RoleDTO>(x =>
                {
                    return String.Equals(x.Name, name, StringComparison.InvariantCultureIgnoreCase);
                });

            return role != null && role.Count() != 0;
        }
    }
}
