﻿using CompanyName.Sklep.Common.Interfaces;
using CompanyName.Sklep.DataAccess.Data.Models;
using CompanyName.Sklep.DataAccess.Data.ModelsDTO;
using CompanyName.Sklep.Services.Interfaces;
using CompanyName.Sklep.Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompanyName.Sklep.Services.Services
{
    public class UserService : IUserService
    {
        IDataAccessContext _context;
        IMapperMiddleware _mapper;
        IHash _hash;

        public UserService(IDataAccessContext context
            ,IMapperMiddleware mapper
            ,IHash hash)
        {
            this._context = context;
            this._mapper = mapper;
            this._hash = hash;
        }

        public UsersListViewModel GetAll()
        {
            var allItems = _context.GetAll<User, UserDTO>();

            UsersListViewModel model = new UsersListViewModel()
            {
                Items = new List<UserViewModel>()
            };

            var users = _context.GetAll<User, UserDTO>();

            model.Items = _mapper.Map<IEnumerable<UserDTO>, IEnumerable<UserViewModel>>(users);

            return model;
        }

        public UserViewModel Get(Int32 id)
        {
            UserViewModel model = null;

            var userDTO = _context.Find<User, UserDTO>(id);

            if (userDTO != null)
                model = _mapper.Map<UserDTO, UserViewModel>(userDTO);

            return model;
        }

        public EditUserViewModel GetForEdit(Int32 id)
        {
            EditUserViewModel model = null;

            var userDTO = _context.Find<User, UserDTO>(id);

            if (userDTO != null)
            {
                model = _mapper.Map<UserDTO, EditUserViewModel>(userDTO);

                var roles = _context.GetAll<Role, RoleDTO>();

                model.Roles = _mapper.Map<IList<RoleDTO>, IList<RoleViewModel>>(roles.ToList());
            }

            return model;
        }

        public EditUserViewModel GetForEdit(UserViewModel data)
        {
            EditUserViewModel model = null;

            if (data != null)
                model = GetForEdit(data.Id);

            return model;
        }

        public Boolean Save(CreateUserViewModel data)
        {
            var saved = false;

            if (data != null)
            {
                data.Password = _hash.Encode(data.Password);
                var userDTO = _mapper.Map<CreateUserViewModel, UserDTO>(data);
                if (!_context.Save<User, UserDTO>(userDTO))
                    data.Message = "No changes where made";
                else
                    saved = true;
            }

            return saved;
        }

        public Boolean Update(EditUserViewModel data)
        {
            var updated = false;

            if (data != null)
            {
                var userDTO = _context.Find<User, UserDTO>(data.Id);

                if (userDTO != null)
                {
                    _mapper.Map<EditUserViewModel, UserDTO>(data, userDTO);
                    userDTO.Role = _context.Find<Role, RoleDTO>(data.SelectedRole);

                    if (!_context.Update<User, UserDTO>(userDTO))
                        data.Message = "No changes where made";
                    else
                        updated = true;
                }
            }

            return updated;
        }

        public Boolean Delete(Int32 id)
        {
            return _context.Delete<User>(id);
        }

        public Boolean Delete(UserViewModel data)
        {
            var deleted = true;

            if (data != null)
                deleted = Delete(data.Id);

            return deleted;
        }
    }
}
