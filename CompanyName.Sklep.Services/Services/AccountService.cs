﻿using CompanyName.Sklep.Common.Interfaces;
using CompanyName.Sklep.DataAccess.Data.Models;
using CompanyName.Sklep.DataAccess.Data.ModelsDTO;
using CompanyName.Sklep.Services.Interfaces;
using CompanyName.Sklep.Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompanyName.Sklep.Services.Services
{
    public class AccountService : IAccountService
    {
        IDataAccessContext _context;
        IMapperMiddleware _mapper;
        IHash _hash;

        public AccountService(IDataAccessContext context, IHash hash, IMapperMiddleware mapper)
        {
            this._context = context;
            this._hash = hash;
            this._mapper = mapper;
        }

        public Boolean IsCredentialsValid(LoginViewModel data)
        {
            var isValid = false;

            UserDTO user = _context.GetAll<User, UserDTO>().SingleOrDefault(x => x.Login == data.Login);

            if (user != null)
            {
                var hashedPassword = _hash.Encode(data.Password);

                isValid = user.Password == hashedPassword;

                user.LastLogin = DateTime.UtcNow;
                _context.Update<User, UserDTO>(user);
            }

            return isValid;
        }

        public ProfileDetailsViewModel GetById(Int32 id)
        {
            ProfileDetailsViewModel model = null;

            model = _mapper.Map<UserDTO, ProfileDetailsViewModel>(_context.Find<User, UserDTO>(id));

            return model;
        }

        public ProfileDetailsViewModel GetByLogin(String login)
        {
            ProfileDetailsViewModel model = null;

            var dbModel = _context.GetAll<User, UserDTO>().SingleOrDefault(x => x.Login == login);

            if (dbModel != null)
                model = _mapper.Map<UserDTO, ProfileDetailsViewModel>(dbModel);

            return model;
        }

        public String GetRole(String login)
        {
            String result = null;

            var user = _context.GetAll<User, UserDTO>().SingleOrDefault(x => x.Login == login);

            if (user != null)
                result = user.Role.Name;

            return result;
        }
    }
}
