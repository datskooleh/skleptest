﻿using AutoMapper;
using CompanyName.Sklep.DataAccess.Data.Models;
using CompanyName.Sklep.DataAccess.Data.ModelsDTO;
using CompanyName.Sklep.Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompanyName.Sklep.Services
{
    public static class Bootstrapper
    {
        public static void Configure() { }

        static Bootstrapper()
        {
            Mapper.Initialize(config =>
            {
                MapMainEntities(config);
                MapRole(config);
                MapUser(config);
                MapArticle(config);
            });
            Mapper.AssertConfigurationIsValid();
        }

        private static void MapMainEntities(IMapperConfiguration config)
        {
            config.CreateMap<User, UserDTO>();
            config.CreateMap<Role, RoleDTO>();
            config.CreateMap<Article, ArticleDTO>();

            config.CreateMap<UserDTO, User>()
                .ForMember(dest => dest.Role, opt => opt.Ignore())
                .ForMember(dest => dest.RoleId, opt => opt.MapFrom(src => src.Role.Id));
            config.CreateMap<RoleDTO, Role>();
            config.CreateMap<ArticleDTO, Article>();
        }

        private static void MapArticle(IMapperConfiguration config)
        {
            config.CreateMap<ArticleDTO, ArticleViewModel>();

            config.CreateMap<ArticleDTO, CreateArticleViewModel>();

            config.CreateMap<ArticleDTO, EditArticlePriceViewModel>()
                .ForMember(dest => dest.Price, opt => opt.Ignore())
                .ForMember(dest => dest.OldPrice, opt => opt.MapFrom(src => src.Price));

            config.CreateMap<List<ArticleDTO>, ArticlesListViewModel>()
                .ForMember(dest => dest.Items, opt => opt.MapFrom(src => src));



            config.CreateMap<ArticleViewModel, ArticleDTO>();

            config.CreateMap<EditArticlePriceViewModel, ArticleDTO>()
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.Description, opt => opt.Ignore())
                .ForMember(dest => dest.AvailableCount, opt => opt.Ignore());

            config.CreateMap<CreateArticleViewModel, ArticleDTO>()
                .ForMember(dest => dest.Id, opt => opt.UseValue<Int32>(0));
        }

        private static void MapUser(IMapperConfiguration config)
        {
            config.CreateMap<UserDTO, UserViewModel>()
                .ForMember(dest => dest.Role, opt => opt.MapFrom(src => src.Role.Name))
                .ForMember(dest => dest.Address, opt => opt.MapFrom(src => src.Country + ", " + src.City + ", " + src.AddressLine1))
                .ForMember(dest => dest.Created, opt => opt.MapFrom(src => src.DateRegistered.ToLocalTime().ToShortDateString()));

            config.CreateMap<UserDTO, ProfileDetailsViewModel>();

            config.CreateMap<UserDTO, CreateUserViewModel>()
                .ForMember(dest => dest.SelectedRole, opt => opt.MapFrom(src => src.Role.Id))
                .ForMember(dest => dest.Roles, opt => opt.Ignore())
                .ForMember(dest => dest.Message, opt => opt.Ignore());

            config.CreateMap<UserDTO, EditUserViewModel>()
                .ForMember(dest => dest.SelectedRole, opt => opt.MapFrom(src => src.Role.Id))
                .ForMember(dest => dest.Roles, opt => opt.Ignore())
                .ForMember(dest => dest.Message, opt => opt.Ignore());

            config.CreateMap<List<UserDTO>, UsersListViewModel>()
                .ForMember(dest => dest.Message, opt => opt.Ignore())
                .ForMember(dest => dest.Items, opt => opt.MapFrom(src => src));



            config.CreateMap<CreateUserViewModel, UserDTO>()
                .ForMember(dest => dest.Id, opt => opt.UseValue<Int32>(0))
                .ForMember(dest => dest.DateRegistered, opt => opt.Ignore())
                .ForMember(dest => dest.LastLogin, opt => opt.Ignore())
                .ForMember(dest => dest.Role, opt => opt.MapFrom(src => src.Roles.SingleOrDefault(x => x.Id == src.SelectedRole)));

            config.CreateMap<EditUserViewModel, UserDTO>()
                .ForMember(dest => dest.Login, opt => opt.Ignore())
                .ForMember(dest => dest.Password, opt => opt.Ignore())
                .ForMember(dest => dest.DateRegistered, opt => opt.Ignore())
                .ForMember(dest => dest.LastLogin, opt => opt.Ignore())
                .ForMember(dest => dest.Role, opt => opt.MapFrom(src => src.Roles.SingleOrDefault(x => x.Id == src.SelectedRole)));

            config.CreateMap<UserViewModel, UserDTO>()
                .ForMember(dest => dest.Password, opt => opt.Ignore())
                .ForMember(dest => dest.Country, opt => opt.Ignore())
                .ForMember(dest => dest.City, opt => opt.Ignore())
                .ForMember(dest => dest.AddressLine1, opt => opt.Ignore())
                .ForMember(dest => dest.AddressLine2, opt => opt.Ignore())
                .ForMember(dest => dest.DateRegistered, opt => opt.Ignore())
                .ForMember(dest => dest.LastLogin, opt => opt.Ignore())
                .ForMember(dest => dest.Role, opt => opt.Ignore());
        }

        private static void MapRole(IMapperConfiguration config)
        {
            config.CreateMap<RoleDTO, RoleViewModel>();

            config.CreateMap<RoleViewModel, RoleDTO>()
                .ForMember(dest => dest.Users, opt => opt.Ignore());
        }
    }
}
