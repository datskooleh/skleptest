﻿using AutoMapper;
using CompanyName.Sklep.Common.Interfaces;
using CompanyName.Sklep.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompanyName.Sklep.Services
{
    public class MapperMiddleware : IMapperMiddleware
    {
        public TDestination Map<TSource, TDestination>(TSource source)
        {
            return Mapper.Map<TSource, TDestination>(source);
        }

        public void Map<T1, T2>(T1 source, T2 destination)
        {
            Mapper.Map<T1, T2>(source, destination);
        }
    }
}
