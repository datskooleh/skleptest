﻿using CompanyName.Sklep.Services.Interfaces;
using FluentValidation;
using FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompanyName.Sklep.Services.Validators
{
    public class DeleteUserValidator : AbstractValidator<Int32?>
    {
        private IUserService _userService;

        public DeleteUserValidator(IUserService userService)
        {
            this._userService = userService;

            RuleFor(x => x)
                .NotNull().WithMessage("Invalid user Id");
        }

        public override ValidationResult Validate(ValidationContext<Int32?> context)
        {
            var validationResult = base.Validate(context);

            if (validationResult.IsValid)
            {
                if (_userService.Get(context.InstanceToValidate.Value).Role.ToLower() == "admin")
                {
                    validationResult.Errors
                        .Add(new ValidationFailure("Message",
                            "Admin can't be removed. Assing another right and then delete."));
                }
            }

            return validationResult;
        }
    }
}
