﻿using CompanyName.Sklep.Services.Interfaces;
using CompanyName.Sklep.Services.Models;
using FluentValidation;
using FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompanyName.Sklep.Services.Validators
{
    public class CreateUserValidator : AbstractValidator<CreateUserViewModel>
    {
        IAccountService _accountService;
        IRoleService _roleService;

        public CreateUserValidator(IAccountService accountService, IRoleService roleService)
        {
            this._accountService = accountService;
            this._roleService = roleService;

            RuleFor(user => user.Login)
                .NotNull().WithMessage("Value can't be empty")
                .Length(4, 25).WithMessage("Lenth must be between 4 and 25");

            RuleFor(user => user.Password)
                .NotNull().WithMessage("Value can't be empty")
                .Length(6, 25).WithMessage("Lenth must be between 6 and 25");

            RuleFor(user => user.SelectedRole)
                .NotEmpty().WithMessage("Value can't be empty")
                .WithMessage("Field is required");

            RuleFor(user => user.FirstName)
                .Length(0, 25).WithMessage("Maximum lenth is 25");

            RuleFor(user => user.LastName)
                .Length(0, 40).WithMessage("Maximum lenth is 40");

            RuleFor(user => user.Country)
                .NotNull().WithMessage("Value can't be empty")
                .Length(0, 25).WithMessage("Maximum lenth is 25");

            RuleFor(user => user.City)
                .NotNull().WithMessage("Value can't be empty")
                .Length(0, 40).WithMessage("Maximum lenth is 40");

            RuleFor(user => user.AddressLine1)
                .NotNull().WithMessage("Value can't be empty")
                .Length(5, 100).WithMessage("Lenth must be between 5 and 100");

            RuleFor(user => user.AddressLine2)
                .Length(0, 100).WithMessage("Maximum lenth is 100");
        }

        public override ValidationResult Validate(ValidationContext<CreateUserViewModel> context)
        {
            var validationResult = base.Validate(context);

            if (validationResult.IsValid)
            {
                if (_accountService.GetByLogin(context.InstanceToValidate.Login) != null)
                    validationResult.Errors
                        .Add(new ValidationFailure("Message", "User already exists in database"));
                if (!_roleService.Exist(context.InstanceToValidate.SelectedRole))
                {
                    validationResult.Errors
                        .Add(new ValidationFailure("SelectedRole", "Role des not exist"));
                }
            }

            return validationResult;
        }
    }
}
