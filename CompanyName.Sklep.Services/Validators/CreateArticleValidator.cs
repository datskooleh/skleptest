﻿using CompanyName.Sklep.Services.Models;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompanyName.Sklep.Services.Validators
{
    public class CreateArticleValidator : AbstractValidator<CreateArticleViewModel>
    {
        public CreateArticleValidator()
        {
            RuleFor(article => article.Name)
                .NotNull().WithMessage("Value can't be empty")
                .Length(2, 50).WithMessage("Length must be between 2 and 50");

            RuleFor(article => article.Description)
                .Length(0, 200).WithMessage("Maximum length is 200");

            RuleFor(article => article.Price)
                .NotEmpty().WithMessage("This field is required")
                .GreaterThanOrEqualTo(0).WithMessage("Price must be a positive number or 0");

            RuleFor(article => article.AvailableCount)
                .NotEmpty().WithMessage("Value is required")
                .InclusiveBetween(0, 10000).WithMessage("Value must be between 0 and 10000");
        }
    }
}
