﻿using CompanyName.Sklep.Services.Interfaces;
using CompanyName.Sklep.Services.Models;
using FluentValidation;
using FluentValidation.Results;

namespace CompanyName.Sklep.Services.Validators
{
    public class EditUserValidator : AbstractValidator<EditUserViewModel>
    {
        IRoleService _roleService;

        public EditUserValidator(IRoleService roleService)
        {
            this._roleService = roleService;

            RuleFor(user => user.SelectedRole)
                .NotEmpty().WithMessage("Value can't be empty");

            RuleFor(user => user.FirstName)
                .Length(0, 25).WithMessage("Maximum lenth is 25");

            RuleFor(user => user.LastName)
                .Length(0, 40).WithMessage("Maximum lenth is 40");

            RuleFor(user => user.Country)
                .NotNull().WithMessage("Value can't be empty")
                .Length(0, 25).WithMessage("Maximum lenth is 25");

            RuleFor(user => user.City)
                .NotNull().WithMessage("Value can't be empty")
                .Length(0, 40).WithMessage("Maximum lenth is 40");

            RuleFor(user => user.AddressLine1)
                .NotNull().WithMessage("Value can't be empty")
                .Length(5, 100).WithMessage("Lenth must be between 5 and 100");

            RuleFor(user => user.AddressLine2)
                .Length(0, 100).WithMessage("Maximum lenth is 100");
        }

        public override ValidationResult Validate(ValidationContext<EditUserViewModel> context)
        {
            var validationResult = base.Validate(context);

            if (validationResult.IsValid)
            {
                if (!_roleService.Exist(context.InstanceToValidate.SelectedRole))
                    validationResult.Errors
                        .Add(new ValidationFailure("SelectedRole", "This role does not exist in database"));
            }

            return validationResult;
        }
    }
}
