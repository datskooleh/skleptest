﻿using CompanyName.Sklep.Services.Interfaces;
using CompanyName.Sklep.Services.Models;
using FluentValidation;
using FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompanyName.Sklep.Services.Validators
{
    public class EditArticlePriceValidator : AbstractValidator<EditArticlePriceViewModel>
    {
        IArticleService _articleService;

        public EditArticlePriceValidator(IArticleService articleService)
        {
            this._articleService = articleService;

            RuleFor(article => article.Price)
                .GreaterThanOrEqualTo(0).WithMessage("Price can't be negative")
                .NotEqual(article => article.OldPrice).WithMessage("New price must be different");
        }

        public override ValidationResult Validate(ValidationContext<EditArticlePriceViewModel> context)
        {
            var validationResult = base.Validate(context);

            if (validationResult.IsValid)
            {
                var model = _articleService.Get(context.InstanceToValidate.Id);

                if (model == null)
                    validationResult.Errors
                        .Add(new ValidationFailure("Message", "Unexpected exception"));
                else if(model.Price == context.InstanceToValidate.Price)
                    validationResult.Errors
                        .Add(new ValidationFailure("Message", "New price must be different"));
            }

            return validationResult;
        }

    }
}
