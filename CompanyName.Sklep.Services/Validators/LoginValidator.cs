﻿using CompanyName.Sklep.Services.Interfaces;
using CompanyName.Sklep.Services.Models;
using FluentValidation;
using FluentValidation.Results;

namespace CompanyName.Sklep.Services.Validators
{
    public class LoginValidator : AbstractValidator<LoginViewModel>
    {
        IAccountService _accountService;

        public LoginValidator(IAccountService accountService)
        {
            this._accountService = accountService;

            RuleFor(user => user.Login).Cascade(FluentValidation.CascadeMode.StopOnFirstFailure)
                .NotNull().WithMessage("This field is required")
                .Length(4, 25).WithMessage("Minimum login lenth is 4 and maximum is 25");

            RuleFor(user => user.Password)
                .NotNull().WithMessage("This field is required")
                .Length(4, 25).WithMessage("Minimum password lenth is 4 and maximum is 25");
        }

        public override ValidationResult Validate(ValidationContext<LoginViewModel> context)
        {
            var validationResult = base.Validate(context);

            if (validationResult.IsValid)
            {
                if (!_accountService.IsCredentialsValid(context.InstanceToValidate))
                {
                    validationResult.Errors
                        .Add(new ValidationFailure("Message", "Invalid login or password"));
                }
            }

            return validationResult;
        }
    }
}
