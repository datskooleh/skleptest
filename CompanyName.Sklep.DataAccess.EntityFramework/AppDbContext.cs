﻿using CompanyName.Sklep.Common.Interfaces;
using CompanyName.Sklep.DataAccess.Data.Interfaces;
using CompanyName.Sklep.DataAccess.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompanyName.Sklep.DataAccess.EntityFramework
{
    public class AppDbContext : IDataAccessContext
    {
        private EFContext context;
        private IMapperMiddleware _mapper;

        public AppDbContext(IMapperMiddleware mapper)
        {
            context = new EFContext();
            this._mapper = mapper;
        }

        public IEnumerable<T2> GetAll<T1, T2>()
            where T1 : class, IEntity
            where T2 : class, IDTO
        {
            return _mapper.Map<IEnumerable<T1>, IEnumerable<T2>>(context.Set<T1>().AsEnumerable());
        }

        public IEnumerable<T2> GetAll<T1, T2>(Func<T2, Boolean> func)
            where T1 : class, IEntity
            where T2 : class, IDTO
        {
            IEnumerable<T2> result = null;

            if (func != null)
                result = _mapper.Map<IEnumerable<T1>, IEnumerable<T2>>(context.Set<T1>().Where(x => func(_mapper.Map<T1, T2>(x))));
            else
                result = GetAll<T1, T2>();

            return result;
        }

        public T2 Find<T1, T2>(Int32 id)
            where T1 : class, IEntity
            where T2 : class, IDTO
        {

            T1 model = context.Set<T1>().SingleOrDefault(x => x.Id == id);

            T2 result = model != null
                ? _mapper.Map<T1, T2>(model)
                : default(T2);

            return result;
        }

        public bool Update<T1, T2>(T2 model)
            where T1 : class, IEntity
            where T2 : class, IDTO
        {
            Boolean updated = false;

            var dbModel = Find<T1>(model.Id);

            if (dbModel != null)
            {
                _mapper.Map(model, dbModel);

                if (context.ChangeTracker.HasChanges())
                {
                    context.Entry(dbModel).State = System.Data.Entity.EntityState.Modified;
                    updated = context.SaveChanges() != 0;
                }
            }

            return updated;
        }

        public bool Save<T1, T2>(T2 model)
            where T1 : class, IEntity
            where T2 : class, IDTO
        {
            var saved = false;

            model.Id = 0;

            T1 dbModel = Activator.CreateInstance<T1>();
            _mapper.Map(model, dbModel);

            var user = dbModel as User;
            if (user != null)
            {
                user.DateRegistered = DateTime.UtcNow;
                user.LastLogin = null;
            }

            dbModel = context.Set<T1>().Add(dbModel);
            context.Entry(dbModel).State = System.Data.Entity.EntityState.Added;
            saved = context.SaveChanges() != 0;

            if (saved)
                model.Id = dbModel.Id;

            return saved;
        }

        public Boolean Delete<T1, T2>(T2 model)
            where T1 : class, IEntity
            where T2 : class, IDTO
        {
            var deleted = false;

            if (model != null)
                deleted = Delete<T1>(model.Id);

            return deleted;
        }

        public Boolean Delete<T>(Int32 id) where T : class, IEntity
        {
            var deleted = false;
            var model = Find<T>(id);

            if (model != null)
            {
                context.Entry(model).State = System.Data.Entity.EntityState.Deleted;
                deleted = context.SaveChanges() != 0;
            }

            if (!deleted)
                deleted = Find<T>(model.Id) == null;

            return deleted;
        }

        private T Find<T>(Int32 id)
            where T : class, IEntity
        {
            return context.Set<T>().SingleOrDefault(x => x.Id == id);
        }
    }
}
