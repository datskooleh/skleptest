﻿using CompanyName.Sklep.DataAccess.Data.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompanyName.Sklep.DataAccess.EntityFramework
{
    [DbConfigurationType(typeof(Config))]
    internal class EFContext : System.Data.Entity.DbContext
    {
        public EFContext()
            : base("StoreDb")
        { }

        public DbSet<Article> Articles { get; set; }

        public DbSet<Role> Roles { get; set; }

        public DbSet<User> Users { get; set; }
    }
}
