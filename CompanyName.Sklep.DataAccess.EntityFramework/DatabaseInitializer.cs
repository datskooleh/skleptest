﻿using CompanyName.Sklep.Common.Classes;
using CompanyName.Sklep.Common.Interfaces;
using CompanyName.Sklep.DataAccess.Data.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompanyName.Sklep.DataAccess.EntityFramework
{
    internal class DatabaseInitializer : CreateDatabaseIfNotExists<EFContext>
    {
        IHash _hash = new PasswordHash();

        protected override void Seed(EFContext context)
        {
            #region Do not remove this lines of code. This two roles and admin MUST exist
            Role roleAdmin = new Role { Name = "admin" };
            roleAdmin = context.Roles.Add(roleAdmin);
            context.SaveChanges();

            Role roleUser = new Role { Name = "user" };
            roleUser = context.Roles.Add(roleUser);
            context.SaveChanges();

            User user = new User()
            {
                Login = "admin",
                Password = _hash.Encode("admin"),
                City = "Lublin",
                Country = "Poland",
                AddressLine1 = "Nadbystrzycka 44A",
                RoleId = roleAdmin.Id
            };
            context.Users.Add(user);
            context.SaveChanges();
            #endregion

            #region You can safely remove all this region
            user = new User()
            {
                Login = "user",
                Password = _hash.Encode("user"),
                City = "Lublin",
                Country = "Poland",
                AddressLine1 = "Nadbystrzycka 44A",
                RoleId = roleUser.Id
            };
            context.Users.Add(user);
            context.SaveChanges();


            var article = new Article()
            {
                Name = "Article 1",
                Description = null,
                Price = 0,
                AvailableCount = 10,
            };
            context.Articles.Add(article);

            article = new Article()
            {
                Name = "Article 2",
                Description = null,
                Price = 364.31M,
                AvailableCount = 34,
            };
            context.Articles.Add(article);
            article = new Article()
            {
                Name = "Article 3",
                Description = "Test description",
                Price = 156.84M,
                AvailableCount = 5,
            };
            context.Articles.Add(article);
            context.SaveChanges();
            #endregion

            base.Seed(context);
        }
    }
}
