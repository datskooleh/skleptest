﻿using CompanyName.Sklep.Common.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompanyName.Sklep.DataAccess.EntityFramework
{
    internal class Config : DbConfiguration
    {
        public Config()
        {
            this.SetDatabaseInitializer(new DatabaseInitializer());
        }
    }
}
